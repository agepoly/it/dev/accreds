use lettre::transport::smtp::authentication::Credentials;
use lettre::{AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor};

static SUBJECT_PASSWORD_RESET: &str = "Whiskey - Password Reset";

#[derive(Debug)]
pub enum MailerError {
    Internal,
    InvalidEmail,
}

pub struct Mailer {}

impl Mailer {
    pub async fn send_password_reset(&self, email: String) -> Result<(), MailerError> {
        let config = crate::utilities::config::get();
        let mailer_config = config.mailer.as_ref().expect("Missing mailer configuration");

        let sender = AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(&mailer_config.smtp_relay)
            .expect("invalid relay")
            .credentials(Credentials::new(mailer_config.smtp_user.clone(), mailer_config.smtp_pass.clone()))
            .build();

        let email = Message::builder()
            .from(mailer_config.from.parse().expect("Invalid from address"))
            .to(email.parse().map_err(|_| MailerError::InvalidEmail)?)
            .subject(SUBJECT_PASSWORD_RESET)
            .body(template_password_reset(&email, "https://whiskey.agepoly.ch/token?t=1234567890"))
            .expect("unable to parse email");

        match sender.send(email).await {
            Ok(res) => {
                println!("send_password_reset: ok, {res:?}");
                Ok(())
            }
            Err(err) => {
                eprintln!("send_password_reset: {err:?}");
                Err(MailerError::Internal)
            }
        }
    }
}

fn template_password_reset(email: &str, url: &str) -> String {
    include_str!("../templates/emails/password_reset.txt")
        .replace("[[EMAIL]]", email)
        .replace("[[URL]]", url)
        .into()
}
