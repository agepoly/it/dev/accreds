// This file was generated with `clorinde`. Do not modify.

#[derive(serde::Serialize, Debug, Clone, PartialEq, serde::Deserialize)]
pub struct Account {
    pub id: uuid::Uuid,
    pub username: String,
    pub created_at: crate::types::time::TimestampTz,
    pub meta_schema: String,
    pub meta_data: serde_json::Value,
}
pub struct AccountBorrowed<'a> {
    pub id: uuid::Uuid,
    pub username: &'a str,
    pub created_at: crate::types::time::TimestampTz,
    pub meta_schema: &'a str,
    pub meta_data: postgres_types::Json<&'a serde_json::value::RawValue>,
}
impl<'a> From<AccountBorrowed<'a>> for Account {
    fn from(
        AccountBorrowed {
            id,
            username,
            created_at,
            meta_schema,
            meta_data,
        }: AccountBorrowed<'a>,
    ) -> Self {
        Self {
            id,
            username: username.into(),
            created_at,
            meta_schema: meta_schema.into(),
            meta_data: serde_json::from_str(meta_data.0.get()).unwrap(),
        }
    }
}
#[derive(serde::Serialize, Debug, Clone, PartialEq, serde::Deserialize)]
pub struct AccountPassword {
    pub id: uuid::Uuid,
    pub username: String,
    pub password: String,
}
pub struct AccountPasswordBorrowed<'a> {
    pub id: uuid::Uuid,
    pub username: &'a str,
    pub password: &'a str,
}
impl<'a> From<AccountPasswordBorrowed<'a>> for AccountPassword {
    fn from(
        AccountPasswordBorrowed {
            id,
            username,
            password,
        }: AccountPasswordBorrowed<'a>,
    ) -> Self {
        Self {
            id,
            username: username.into(),
            password: password.into(),
        }
    }
}
use crate::client::async_::GenericClient;
use futures::{self, StreamExt, TryStreamExt};
pub struct AccountQuery<'c, 'a, 's, C: GenericClient, T, const N: usize> {
    client: &'c C,
    params: [&'a (dyn postgres_types::ToSql + Sync); N],
    stmt: &'s mut crate::client::async_::Stmt,
    extractor: fn(&tokio_postgres::Row) -> AccountBorrowed,
    mapper: fn(AccountBorrowed) -> T,
}
impl<'c, 'a, 's, C, T: 'c, const N: usize> AccountQuery<'c, 'a, 's, C, T, N>
where
    C: GenericClient,
{
    pub fn map<R>(self, mapper: fn(AccountBorrowed) -> R) -> AccountQuery<'c, 'a, 's, C, R, N> {
        AccountQuery {
            client: self.client,
            params: self.params,
            stmt: self.stmt,
            extractor: self.extractor,
            mapper,
        }
    }
    pub async fn one(self) -> Result<T, tokio_postgres::Error> {
        let stmt = self.stmt.prepare(self.client).await?;
        let row = self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    }
    pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error> {
        self.iter().await?.try_collect().await
    }
    pub async fn opt(self) -> Result<Option<T>, tokio_postgres::Error> {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self
            .client
            .query_opt(stmt, &self.params)
            .await?
            .map(|row| (self.mapper)((self.extractor)(&row))))
    }
    pub async fn iter(
        self,
    ) -> Result<
        impl futures::Stream<Item = Result<T, tokio_postgres::Error>> + 'c,
        tokio_postgres::Error,
    > {
        let stmt = self.stmt.prepare(self.client).await?;
        let it = self
            .client
            .query_raw(stmt, crate::slice_iter(&self.params))
            .await?
            .map(move |res| res.map(|row| (self.mapper)((self.extractor)(&row))))
            .into_stream();
        Ok(it)
    }
}
pub struct AccountPasswordQuery<'c, 'a, 's, C: GenericClient, T, const N: usize> {
    client: &'c C,
    params: [&'a (dyn postgres_types::ToSql + Sync); N],
    stmt: &'s mut crate::client::async_::Stmt,
    extractor: fn(&tokio_postgres::Row) -> AccountPasswordBorrowed,
    mapper: fn(AccountPasswordBorrowed) -> T,
}
impl<'c, 'a, 's, C, T: 'c, const N: usize> AccountPasswordQuery<'c, 'a, 's, C, T, N>
where
    C: GenericClient,
{
    pub fn map<R>(
        self,
        mapper: fn(AccountPasswordBorrowed) -> R,
    ) -> AccountPasswordQuery<'c, 'a, 's, C, R, N> {
        AccountPasswordQuery {
            client: self.client,
            params: self.params,
            stmt: self.stmt,
            extractor: self.extractor,
            mapper,
        }
    }
    pub async fn one(self) -> Result<T, tokio_postgres::Error> {
        let stmt = self.stmt.prepare(self.client).await?;
        let row = self.client.query_one(stmt, &self.params).await?;
        Ok((self.mapper)((self.extractor)(&row)))
    }
    pub async fn all(self) -> Result<Vec<T>, tokio_postgres::Error> {
        self.iter().await?.try_collect().await
    }
    pub async fn opt(self) -> Result<Option<T>, tokio_postgres::Error> {
        let stmt = self.stmt.prepare(self.client).await?;
        Ok(self
            .client
            .query_opt(stmt, &self.params)
            .await?
            .map(|row| (self.mapper)((self.extractor)(&row))))
    }
    pub async fn iter(
        self,
    ) -> Result<
        impl futures::Stream<Item = Result<T, tokio_postgres::Error>> + 'c,
        tokio_postgres::Error,
    > {
        let stmt = self.stmt.prepare(self.client).await?;
        let it = self
            .client
            .query_raw(stmt, crate::slice_iter(&self.params))
            .await?
            .map(move |res| res.map(|row| (self.mapper)((self.extractor)(&row))))
            .into_stream();
        Ok(it)
    }
}
pub fn all_accounts() -> AllAccountsStmt {
    AllAccountsStmt(crate::client::async_::Stmt::new(
        "SELECT id,username,created_at,meta_schema,meta_data FROM accounts",
    ))
}
pub struct AllAccountsStmt(crate::client::async_::Stmt);
impl AllAccountsStmt {
    pub fn bind<'c, 'a, 's, C: GenericClient>(
        &'s mut self,
        client: &'c C,
    ) -> AccountQuery<'c, 'a, 's, C, Account, 0> {
        AccountQuery {
            client,
            params: [],
            stmt: &mut self.0,
            extractor: |row| AccountBorrowed {
                id: row.get(0),
                username: row.get(1),
                created_at: row.get(2),
                meta_schema: row.get(3),
                meta_data: row.get(4),
            },
            mapper: |it| Account::from(it),
        }
    }
}
pub fn account_by_username() -> AccountByUsernameStmt {
    AccountByUsernameStmt(crate::client::async_::Stmt::new(
        "SELECT id,username,password FROM accounts WHERE username = $1",
    ))
}
pub struct AccountByUsernameStmt(crate::client::async_::Stmt);
impl AccountByUsernameStmt {
    pub fn bind<'c, 'a, 's, C: GenericClient, T1: crate::StringSql>(
        &'s mut self,
        client: &'c C,
        username: &'a T1,
    ) -> AccountPasswordQuery<'c, 'a, 's, C, AccountPassword, 1> {
        AccountPasswordQuery {
            client,
            params: [username],
            stmt: &mut self.0,
            extractor: |row| AccountPasswordBorrowed {
                id: row.get(0),
                username: row.get(1),
                password: row.get(2),
            },
            mapper: |it| AccountPassword::from(it),
        }
    }
}
