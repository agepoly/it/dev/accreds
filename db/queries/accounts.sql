--: Account() : serde::Deserialize
--: AccountPassword() : serde::Deserialize

--! all_accounts : Account
SELECT id,username,created_at,meta_schema,meta_data FROM accounts;

--! account_by_username (username) : AccountPassword
SELECT id,username,password FROM accounts WHERE username = :username;