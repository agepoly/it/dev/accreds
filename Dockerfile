FROM debian:bookworm AS runtime
RUN apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates libssl-dev curl unzip

FROM rust:latest AS rust_build
RUN cargo install cargo-build-deps
RUN cargo new app

WORKDIR /app

# cargo build-deps requires the whole dependency to be available
COPY macros/ ./macros/
COPY Cargo.toml Cargo.lock ./
RUN cargo build-deps --release

COPY . ./
RUN cargo build --release

FROM runtime
WORKDIR /app
COPY migrations ./migrations
COPY --from=rust_build /app/target/release/whiskey /usr/local/bin/whiskey-api
CMD ["/usr/local/bin/whiskey-api"]
