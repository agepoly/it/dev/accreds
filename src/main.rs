use config::Config;
use http::request;
use tonic::transport::Server;
use tonic::{Request, Response, Status};

use utilities::database::Database;
use whiskey_api::whiskey_api_server::{WhiskeyApi, WhiskeyApiServer};
use whiskey_api::{AppCustomError, AppCustomErrorType, Empty, LoginWithPasswordRequest, SignUpRequest, WhiskeyConfig};

pub mod whiskey_api {
    tonic::include_proto!("whiskey");
}

pub mod utilities;

pub struct WhiskeyApiService {
    pub db: utilities::database::Database,
}

#[tonic::async_trait]
impl WhiskeyApi for WhiskeyApiService {
    async fn get_whiskey_config(&self, _request: Request<Empty>) -> Result<Response<WhiskeyConfig>, Status> {
        println!("get whiskey config");

        Ok(Response::new(WhiskeyConfig {
            version: "0.1.0".to_owned(),
            welcome_message: None, // Some("Ce message provient du serveur ;)".to_owned()),
        }))
    }

    async fn sign_up(&self, request: Request<SignUpRequest>) -> Result<Response<Empty>, Status> {
        println!("sign_up");

        use utilities::mailer::Mailer;
        let mailer = Mailer {};
        match mailer.send_password_reset(request.get_ref().email.clone()).await {
            Ok(()) => Ok(Response::new(Empty {})),
            Err(err) => Err(Status::internal(format!("{err:?}"))),
        }
    }

    async fn login_with_password(&self, request: Request<LoginWithPasswordRequest>) -> Result<Response<Empty>, Status> {
        println!("login with password");

        match self.db.account_by_username(request.get_ref().username.clone()).await {
            Ok(account) => {
                if account.password == request.get_ref().password {
                    Ok(Response::new(Empty {}))
                } else {
                    Err(Status::permission_denied("Bad credentials (password)"))
                }
            }
            Err(_err) => Err(Status::permission_denied("Bad credentials (username)")),
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = utilities::config::get();
    let port = config.server.as_ref().map(|s| s.port).flatten().unwrap_or(10000);
    let addr = format!("[::1]:{}", port).parse().unwrap();

    let (client, connection) = tokio_postgres::connect(&config.database.config, tokio_postgres::NoTls)
        .await
        .unwrap();

    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    println!("WhiskeyApiServer listening on: {}", port);

    let api_svc = WhiskeyApiService { db: Database::new(client) };
    let api_server = WhiskeyApiServer::new(api_svc);

    Server::builder()
        .accept_http1(true)
        .add_service(tonic_web::enable(api_server))
        .serve(addr)
        .await?;

    Ok(())
}
