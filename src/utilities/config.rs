use std::sync::OnceLock;

use config::Config;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct WhiskeyServerConfig {
    pub port: Option<u32>,
}

#[derive(Deserialize)]
pub struct WhiskeyMailerConfig {
    pub from: String,
    pub smtp_relay: String,
    pub smtp_user: String,
    pub smtp_pass: String,
}

#[derive(Deserialize)]
pub struct WhiskeyDatabaseConfig {
    pub config: String,
}

#[derive(Deserialize)]
pub struct WhiskeyConfig {
    pub server: Option<WhiskeyServerConfig>,
    pub mailer: Option<WhiskeyMailerConfig>,
    pub database: WhiskeyDatabaseConfig,
}

pub fn get() -> &'static WhiskeyConfig {
    static CONFIG: OnceLock<WhiskeyConfig> = OnceLock::new();
    CONFIG.get_or_init(|| {
        let raw = Config::builder()
            .add_source(config::File::with_name("settings").required(false))
            .add_source(config::Environment::with_prefix("WHISKEY").separator("_").ignore_empty(true))
            .build()
            .expect("Invalid config source");
        raw.try_deserialize::<WhiskeyConfig>().expect("Invalid config format")
    })
}
