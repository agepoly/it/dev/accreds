-- Active: 1721123333381@@10.144.0.1@5432@whiskey3
-- migrate:up
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE IF NOT EXISTS accounts
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    username VARCHAR(50),
    password TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    meta_schema VARCHAR(255),
    meta_data JSONB,
    
    CONSTRAINT accounts_pk PRIMARY KEY (id)
);

-- migrate:down

DROP TABLE IF EXISTS accounts;
DROP EXTENSION pgcrypto;