use clorinde::queries::accounts::{self, AccountPassword};

#[derive(Debug)]
pub enum DatabaseError {
    Internal,
    NotFound
}

pub struct Database {
    client: tokio_postgres::Client
}

impl Database {
    pub fn new(client: tokio_postgres::Client) -> Self {
        Self {
            client
        }
    }

    pub async fn account_by_username(&self, username: String) -> Result<AccountPassword, DatabaseError> {
        match accounts::account_by_username().bind(&self.client, &username).one().await {
            Ok(account) => {
                Ok(account)
            },
            Err(err) => {
                eprintln!("Database error: {err:?}");
                Err(DatabaseError::Internal)
            }
        }
    }
}