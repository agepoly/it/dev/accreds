# Whiskey

New (2022) accrediation backend for AGEPoly

## Development setup

1. ### Setup a local PostgreSQL server
   1. Install PostgreSQL : either directly (easier) or via a Docker image
   2. `sudo su postgres` to impersonate the admin user of PostgreSQL
   3. `createuser -W whiskey` to create a user named whiskey for our server, type `whiskey` as in the password prompt
   4. `createdb -O whiskey whiskey` to create a user named whiskey for our server, -O to set it as the owner of the db
   
2. #### Install the Rust toolchain, easily done with `rustup`
3. ### Initialize the server
   1. Initialize the Crate by opening your IDE or with `cargo b|c|r` (one of b=build, c=check, r=run). This will download and compile the dependencies. There will be some errors because SQLx is unable to use the database at the moment to compile-check the queries.
   2. Install sqlx-cli with `cargo install sqlx-cli`
   3. Run `sqlx migrate run --database-url postgres://whiskey:whiskey@localhost/whiskey` to run the migrations on the server. This is required if you want to fully compile. Notice the URL parameter. When compiling with the IDE, provide the URL as an env variable DATABASE_URL.
   4. You can use `clear && DATABASE_URL="postgres://whiskey:whiskey@localhost/whiskey" cargo c` to check if the errors are gone
4. ### Important notes
- When you compile, SQLx has two modes : online or offline. With the online mode the compiler will check against your live local database. With offline the sqlx-data.json file is used as a cache. If you build with the env var DATABASE_URL set, it will use the online mode and offline otherwise. To regenerate the cache use `cargo sqlx prepare --database-url postgres://whiskey:whiskey@localhost/whiskey`
5. ### Tips
   - Notice the sql-scripts folder to quickly inject mock data in the database. Most IDE provide tools to manage your local dev database. Add a connection to run the scripts. On JetBrains' IDEs : Ctrl+Enter to run the selected line


## Deployment guidelines *[WIP]*

Files and folders related to deployment:
- Dockerfile (describes how to build a Docker image to be run on the cluster)
- .gitlab-ci.yml (describe what Gitlab should do as part of the CI/CD process)
- sqlx-data.json (caches the state of the database for SQLx in offline mode used by Gitlab CI)

### General information

This project (whiskey-api) is related to its frontend (whiskey-app) but at this time of writing the frontend is provided
programmatically by the backend and doesn't require any intervention at the infrastructure level. 

Whiskey's backend requires the deployment of:
- a PostgreSQL database. Migrations are automatically applied on a new database and can otherwise be managed via `sqlx-cli`.
- an optional instance of [Ory Hydra](https://www.ory.sh/hydra/) used to provide the OAuth 2.0 and OpenID Connect API to clients. It also uses it's own PostgreSQL database.

The backend itself is provided as a Docker image [hosted on Gitlab](https://gitlab.com/agepoly/it/dev/whiskey-api/container_registry) built with the Dockerfile.

Order of execution when a push to main occurs :
1. .gitlab-ci.yml triggers a new Gitlab Pipeline. Two templates are used for building the Docker image and the deployment on the Kubernetes cluster.
2. The Dockerfile is used to build the image that is then pushed to Gitlab repository to be later retrieved by Kubernetes.
3. The next stage is the compilation of the Helm Chart templates to generate the final Kubernetes manifests.
4. Two "agepoly-deployments" are created : one for the backend and one to deploy Ory Hydra.

### Data models

[Click here for the interactive database model](https://azimutt.agepoly.ch/59e90c25-6e24-4b44-85cf-a7d6f0f53eb2/b8a2fae8-528d-4d6f-8a03-1440650e50e1?layout=initial%20layout&token=ee18e841-ba0f-4be2-8e9d-1754d4604f68)

### Environment variables

- [x] `DATABASE_URL=postgres://$(DB_USER):$(DB_PASSWORD)@$(DB_HOST):$(DB_PORT)/$(DB_NAME)` is the DSN required by SQLx for the database connection. It is computed from the env variables provided by the infrastructure (Helm Chart agepoly-deployment).
- [ ] `ROCKET_PORT=4000`
- [ ] `HYDRA_ADMIN_ENDPOINT` is the endpoint (up to the http port number) of the Hydra Admin API. 
- [ ] `ROCKET_LOG_LEVEL`
- [ ] `ROCKET_ADDRESS=0.0.0.0` the listening address for Rocket (`0.0.0.0` is the wildcard) 
